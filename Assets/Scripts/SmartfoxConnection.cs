﻿using UnityEngine;
using System.Collections;
using Sfs2X;
using Sfs2X.Entities;
using Sfs2X.Requests;
public class SmartfoxConnection : MonoBehaviour
{
    private static SmartfoxConnection m_instance;
    public SmartFox sfs;
    public int currentJoinedRoom;
    public User myInfo;
    public bool isRoomHost= false;
    public static SmartfoxConnection Instance
    {
        get
        {
            return m_instance;
        }
    }
    void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (sfs != null)
            sfs.ProcessEvents();
    }
    void OnApplicationQuit()
    {
        if (sfs != null && sfs.IsConnected)
        {
            sfs.Disconnect();
        }
    }

    // Disconnect from the socket when ordered by the main Panel scene
    // ** Important for Windows users - can cause crashes otherwise
    public void Disconnect()
    {
        OnApplicationQuit();
    }
    public void SendPublicMessage(string message)
    {
        // Send a public message
        sfs.Send(new PublicMessageRequest(message));
    }
    public void SendPrivateMessage(string mes, int userID)
    {
        sfs.Send(new PrivateMessageRequest(mes, userID));

    }
}
