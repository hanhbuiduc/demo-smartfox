﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sfs2X.Entities;
using System.Collections.Generic;
using Sfs2X.Core;
using Sfs2X.Requests;
public class CanvasGamePlay : MonoBehaviour {
    public Text textNameRoom;
    public List<SlotGamePlayInfo> listSlotInGame;
    Room roomInfo;
    int mySlot;
    public Text textBtnReady;
    public GameObject btnReady;
    public GameObject btnRePlay;
    public GameObject btnOutRoom;

    int numUserisRandom = 0;
	// Use this for initialization
	void Start () {
        roomInfo = SmartfoxConnection.Instance.sfs.GetRoomById(SmartfoxConnection.Instance.currentJoinedRoom);
        textNameRoom.text = roomInfo.Name;
        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, OnUserEnterRoom);
        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, OnPublicMessage);
        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.PRIVATE_MESSAGE, OnPrivateMessage);
        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.USER_EXIT_ROOM, OnUserExitRoom);

        if (SmartfoxConnection.Instance.isRoomHost)
        {
            textBtnReady.text = "Start";
        }
        else
        {
            textBtnReady.text = "Ready";
        }
        btnReady.SetActive(true);
        btnRePlay.SetActive(false);
        GetSlotInfo();
       // GetData();  
	}


    
    void OnDestroy()
    {
        //SmartfoxConnection.Instance.sfs.RemoveEventListener(SFSEvent.USER_ENTER_ROOM, OnUserEnterRoom);

    }
    public void RePlay()
    {
        numUserisRandom = 0;
        btnReady.SetActive(true);
        btnRePlay.SetActive(false);
        foreach (SlotGamePlayInfo slot in listSlotInGame)
        {
            if (!slot.isEmpty)
            {
                slot.ClearSlot();
            }
        }
        if (SmartfoxConnection.Instance.isRoomHost)
        {
            foreach (SlotGamePlayInfo slot in listSlotInGame)
            {
                if (!slot.isEmpty)
                {
                    SmartfoxConnection.Instance.SendPrivateMessage("Newgame-",slot.user.Id);
                }
            }
        }
        
    }
	// Update is called once per frame
	void Update () {
     
       
	}
    public void GetSlotInfo()
    {
        for (int i = roomInfo.UserList.Count-1; i >= 0; i--)
        {
            int idSlot = AddSlot(roomInfo.UserList[i]);
            if (roomInfo.UserList[i].IsItMe)
            {
                mySlot = idSlot;
            }
        }
        
    }
    public int AddSlot(User user)
    {
        foreach (SlotGamePlayInfo slot in listSlotInGame)
        {
            if (slot.isEmpty)
            {
                if (user.IsItMe)
                {
                    slot.SetData(user, SmartfoxConnection.Instance.isRoomHost);

                }else
                    slot.SetData(user);

                slot.isEmpty = false;
                return slot.idSlot;
            }
        }
        return -1;
    }

    void OnUserEnterRoom(BaseEvent evt)
    {
        Debug.Log("cos nguoi vao room");
        Room room = (Room)evt.Params["room"];
        User user = (User)evt.Params["user"];
        if (SmartfoxConnection.Instance.isRoomHost)
        {
            SmartfoxConnection.Instance.SendPrivateMessage("Host-"+SmartfoxConnection.Instance.myInfo.Id,user.Id);
        }
        if (room.Id==roomInfo.Id)
        {
            AddSlot(user);
            
        }
    }
    void OnPublicMessage(BaseEvent evt)
    {
        
    }
    public void OnPrivateMessage(BaseEvent evt)
    {
        User sender = (User)evt.Params["sender"];
        string message = (string)evt.Params["message"];
        Debug.Log("mes" + message);
        CheckMessage(message, sender);

       
    }
    public void SendMessageReady()
    {
        foreach (SlotGamePlayInfo slot in listSlotInGame)
        {
            if (!slot.isEmpty && !slot.user.IsItMe)
            {
                SmartfoxConnection.Instance.SendPrivateMessage("Ready-", slot.user.Id);
            }
        }
    }
    public void CheckMessage(string mes,User sender=null)
    {
        string[] mesSplit = mes.Split('-');
        SlotGamePlayInfo slot1 = GetSlotFromUser(sender.Id);

        if (mesSplit.Length==0)
        {
            return;
        }
        switch (mesSplit[0])
        {
            case "Ready":
                    slot1.SetReady(true);
                    if (sender.IsItMe)
                        btnReady.SetActive(false);
                break;
            case "Score":
                    slot1.numCard1 = int.Parse(mesSplit[1]);
                    slot1.numCard2 = int.Parse(mesSplit[2]);
                    slot1.numCard3 = int.Parse(mesSplit[3]);
                    slot1.ShowNumCards();
                    if (SmartfoxConnection.Instance.isRoomHost)
                    {
                       numUserisRandom++;
                       Debug.Log("numUserisRandom" + numUserisRandom);
                       if (numUserisRandom==roomInfo.UserList.Count)
                       {
                           CheckWinForRoomHost();
                       }
                    }
                    if (!listSlotInGame[mySlot].isRandom)
                    {
                        SendScoreCard();
                    }
                   
                break;
            case "Winner":
                    int highScore = int.Parse(mesSplit[1]);
                    foreach (SlotGamePlayInfo item in listSlotInGame)
                    {
                        if (!item.isEmpty)
                        {
                            if (item.TotalScore() == highScore)
                            {
                                item.SetWinner(true);
                            }
                            else
                            {
                                item.SetWinner(false);
                            }
                        }
                    }
                    if (SmartfoxConnection.Instance.isRoomHost)
                    {
                        btnRePlay.SetActive(true);
                    }
                    btnOutRoom.SetActive(true);
                    break;
            case "Host":
                    if (sender.IsItMe)
                    {
                        return;
                    }
                    int userID = int.Parse(mesSplit[1]);
                    
                    slot1 = GetSlotFromUser(userID);
                    slot1.SetHoster();                    
                    break;  
            case "Newgame":
                    if (!sender.IsItMe)
                    {
                        RePlay();
                    }
                    break;
            default: break;
        }
    }
    public void CheckWinForRoomHost()
    {
        int highScore = 0;
        foreach (SlotGamePlayInfo slot in listSlotInGame)
        {
            if (!slot.isEmpty)
            {
                if (highScore < slot.TotalScore())
                {
                    highScore = slot.TotalScore();
                }
            }
        }
        SendHightScore(highScore);
            
    }
    SlotGamePlayInfo GetSlotFromUser(int userID)
    {
        foreach (SlotGamePlayInfo slot in listSlotInGame)
        {
            if (slot.user.Id==userID)
            {
                return slot;
            }
        }
        return null;
    }
    public void SendHightScore(int hightScore)
    {
        string mes = "Winner-" + hightScore;
        foreach (SlotGamePlayInfo slot in listSlotInGame)
        {
            if (!slot.isEmpty)
            {
                SmartfoxConnection.Instance.SendPrivateMessage(mes, slot.user.Id);
            }
        }
    }
    public void SendWhoIsWinner(int idUser)
    {
        string mes = "Winner-" + idUser;
        foreach (SlotGamePlayInfo slot in listSlotInGame)
        {
            if (!slot.isEmpty && !slot.user.IsItMe)
            {
                SmartfoxConnection.Instance.SendPrivateMessage(mes, slot.user.Id);
            }
        }
    }
    public void  SendScoreCard()
    {
        listSlotInGame[mySlot].RandomCard();
        string mesScore = "Score-"+ listSlotInGame[mySlot].numCard1 + "-" + listSlotInGame[mySlot].numCard2 + "-" + listSlotInGame[mySlot].numCard3;
        foreach (SlotGamePlayInfo slot in listSlotInGame)
        {
            if (!slot.isEmpty && !slot.user.IsItMe)
            {                
                SmartfoxConnection.Instance.SendPrivateMessage(mesScore, slot.user.Id);
            }
        }
    }
    public void BtnReady_OnClick()
    {
        Debug.Log("start game" + SmartfoxConnection.Instance.isRoomHost);
        if (SmartfoxConnection.Instance.isRoomHost)
        {
            if (roomInfo.UserCount<2)
            {
                return;
            }
            foreach (SlotGamePlayInfo slot in listSlotInGame)
            {
                if (!slot.isEmpty)
                {
                    if (!slot.isReady && !slot.user.IsItMe)
                    {
                        return;
                    }                    
                }
            }
            btnReady.SetActive(false);
            btnOutRoom.SetActive(false);
            Debug.Log("sendScore game" + SmartfoxConnection.Instance.isRoomHost);
            SendScoreCard();
        }else
        {
            SendMessageReady();
          //  btnReady.SetActive(false);

        }
    }
    public void LogOutRoom()
    {
        SmartfoxConnection.Instance.sfs.Send(new LeaveRoomRequest());
    }
    private void OnUserExitRoom(BaseEvent evt)
    {
        User user = (User)evt.Params["user"];
        
        Room room = (Room)evt.Params["room"];
        if (room.Id==roomInfo.Id)
        {
            foreach (SlotGamePlayInfo slot in listSlotInGame)
            {
                if (!slot.isEmpty)
                {
                    if (user.Id==slot.user.Id)
                    {
                        slot.ClearSlot();
                        slot.isEmpty = true;
                        slot.textNameUser.text = "";
                        numUserisRandom = 0;
                    }
                }
            }
        }
       
        if (user.IsItMe)
        {

            Application.LoadLevel("StartGame");
        }
       
          
    }
}

