﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sfs2X.Core;
using Sfs2X.Requests;
using System.Collections.Generic;
using Sfs2X.Entities;
public class PanelRoom : MonoBehaviour {

    public GameObject roomPrefab;
    public Transform contentRoom;
    public InputField nameRoom;
    public List<Room> listRooms;
	// Use this for initialization
	void Start () {
        
        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoined);
        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnJoinError);
        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.ROOM_ADD, OnRoomAdded);
        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.ROOM_REMOVE, OnRoomRemoved);

        SmartfoxConnection.Instance.sfs.AddEventListener(SFSEvent.ROOM_CREATION_ERROR, OnRoomCreationError);
        GetListRoom();
	}

    private void OnRoomRemoved(BaseEvent evt)
    {
        GetListRoom();
    }

    
	// Update is called once per frame
	void Update () {
	
	}
    public void GetListRoom()
    {
       listRooms = SmartfoxConnection.Instance.sfs.RoomList;
       StartCoroutine(SpawnRoom());
    }
    IEnumerator SpawnRoom()
    {
        int index = 0;
        foreach (var child in listRooms)
        {
            
            Room room = listRooms[index];
            if (index < contentRoom.childCount)
            {
                RoomData roomData = contentRoom.GetChild(index).GetComponent<RoomData>();
                roomData.SetData(room.Name, room.Id, this);
            //   contentRoom.GetChild(index).GetComponent<RoomData>().SetData(room.Name,room.Id,this);
               roomData.btnJoin.onClick.AddListener(() => JoinGame_OnClick(room.Id));

            }
            else
            {
                Transform newGO = Instantiate(roomPrefab).transform;
                newGO.SetParent(contentRoom);
                newGO.localScale = Vector3.one;
                RoomData roomData = newGO.GetComponent<RoomData>();
                roomData.SetData(room.Name, room.Id, this);
                roomData.btnJoin.onClick.AddListener(()=>JoinGame_OnClick(room.Id));
            }
            index++;
            yield return new WaitForEndOfFrame();
        }
        for (int i = index; i < contentRoom.childCount; i++)
        {
            contentRoom.GetChild(i).gameObject.SetActive(false);
        }
    }
    public void JoinGame_OnClick(int idRoom)
    {
        Debug.Log("click");
        
        SmartfoxConnection.Instance.sfs.Send(new JoinRoomRequest(idRoom));
        SmartfoxConnection.Instance.currentJoinedRoom = idRoom;
        SmartfoxConnection.Instance.isRoomHost = false;

    }
    public void BtnCreateRoom_OnClick()
    {
        
        // Create a new Chat Room
        RoomSettings settings = new RoomSettings(nameRoom.text);
        settings.MaxUsers = 3;
        settings.GroupId = "games";
        settings.IsGame = true;

        SmartfoxConnection.Instance.sfs.Send(new CreateRoomRequest(settings,true));
        Debug.Log("create");

    }
    
    public void ShowListUsersOnline()
    {
       

    }
    //Event Smartfox
    void OnRoomAdded(BaseEvent e)
    {
      Room newRoom=  (Room)e.Params["room"];
        
      SmartfoxConnection.Instance.currentJoinedRoom = newRoom.Id;
      SmartfoxConnection.Instance.isRoomHost = true;
        GetListRoom();
        Debug.Log("listRooms" + listRooms.Count);
    }
    void OnRoomCreationError(BaseEvent e)
    {
        
    }
    void OnJoined(BaseEvent e)
    {
        Application.LoadLevel("GamePlay");
    }
    void OnJoinError(BaseEvent e)
    {

    }
}
