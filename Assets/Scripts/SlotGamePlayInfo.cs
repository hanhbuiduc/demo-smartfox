﻿using UnityEngine;
using System.Collections;
using Sfs2X.Entities;
using UnityEngine.UI;
public class SlotGamePlayInfo : MonoBehaviour {
    public int idSlot;
    public bool isReady=false;
    public bool isEmpty = true;
    public bool isRandom = false;
    public bool isHoster = false;
    public User user;
    public int numCard1;
    public int numCard2;
    public int numCard3;
    public Text textNameUser;
    public Text textNum1;
    public Text textNum2;
    public Text textNum3;
    public Text textReady;
    public void SetData(User _user, bool _isHoster=false)
    {
        this.user = _user;
        SetReady(false);
        isHoster = _isHoster;
        if (isHoster)
        {
            textNameUser.color = Color.red;
        }
        else
        {
            textNameUser.color = Color.black;
        }
        ShowData();
    }
    public void SetHoster()
    {
        isHoster = true;
        if (isHoster)
        {
            textNameUser.color = Color.red;
        }
        else
        {
            textNameUser.color = Color.black;
        }
    }
    public void ShowData()
    {
        textNameUser.text = user.Name;
    }
    public void RandomCard()
    {
        numCard1 = Random.Range(1, 11);
        numCard2 = Random.Range(1, 11);
        numCard3 = Random.Range(1, 11);
        isRandom = true;
        ShowNumCards();
    }
    public void ShowNumCards()
    {
        textNum1.text = numCard1.ToString();
        textNum2.text = numCard2.ToString();
        textNum3.text = numCard3.ToString();        

    }
   
    public void SetReady(bool _isReady)
    {
        this.isReady = _isReady;

        if (_isReady)
        {
            textReady.text = "Ready";
        }else
            textReady.text = "Waiting";
        
    }
    public int TotalScore()
    {
        return (numCard1 + numCard2 + numCard3);
    }
    public void SetWinner(bool isWinner)
    {
        if (isWinner)
        {
            textReady.text = "Win";
        }
        else
        {
            textReady.text = "Lost";
        }
    }
    public void ClearSlot()
    {
        isReady=false;
        isRandom = false;
        textNum1.text = string.Empty;
        textNum2.text = string.Empty;
        textNum3.text = string.Empty;
        if (isHoster)
        {
          textReady.text = "";
        }else
            textReady.text = "waiting";

    }
}
