﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
public class LoginGame : MonoBehaviour {

    public string hostIp = "127.0.0.1";
    public int port = 9933;
    public string zone = "BasicExamples";
    public static SmartFox smartFox;
    public InputField username;
    public Text log;
    public PanelRoom panelRoom;
    void Start()
    {
        if (SmartfoxConnection.Instance.sfs!=null)
        {
            panelRoom.gameObject.SetActive(true);
        }
        else
        {
            StartCoroutine(ProcessEvents());
        }
    }
    // Use this for initialization
    void Update()
    {
      
    }
    IEnumerator ProcessEvents(){
        while (true)
        {
            if (smartFox != null)
                smartFox.ProcessEvents();
            yield return new WaitForEndOfFrame();
        }
    }
    // Handle disconnection automagically
    // ** Important for Windows users - can cause crashes otherwise
    void OnDestroy()
    {
        smartFox.RemoveAllEventListeners();

    }
    void OnApplicationQuit()
    {
        if (smartFox != null && smartFox.IsConnected)
        {
            smartFox.Disconnect();
        }
    }

    // Disconnect from the socket when ordered by the main Panel scene
    // ** Important for Windows users - can cause crashes otherwise
    public void Disconnect()
    {
        OnApplicationQuit();
    }
	
    //event
    void OnConection(BaseEvent e)
    {
        Debug.Log("connection");
        bool isSuccess = (bool)e.Params["success"];
        if (isSuccess)
        {
            SmartfoxConnection.Instance.sfs = smartFox;
            smartFox.Send(new Sfs2X.Requests.LoginRequest(username.text));
        }
       
    }
    void OnConnectionLost(BaseEvent e)
    {
        smartFox.RemoveAllEventListeners();
        string reason = (string)e.Params["reason"];
        log.text += " \n connection fail: " + reason;
     //   Debug.Log("Lost connection " + reason);
    }
    void OnLogin(BaseEvent e)
    {
        log.text += " \n Login successfull";
        Debug.Log("login successfull");
        SmartfoxConnection.Instance.myInfo = smartFox.MySelf;
        panelRoom.gameObject.SetActive(true);
        
    }
    void OnLoginError(BaseEvent e)
    {
        smartFox.RemoveAllEventListeners();
        smartFox.Disconnect();
        log.text += " \n Login failed: " + (string)e.Params["errorMessage"];
    }
    public void OnLoginButtonClick() {
        ConfigData cfg = new ConfigData();
        cfg.Host = hostIp;
        cfg.Port = port;
        cfg.Zone = zone;
        smartFox = new SmartFox();
        smartFox.AddEventListener(SFSEvent.CONNECTION, OnConection);
        smartFox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        smartFox.AddEventListener(SFSEvent.LOGIN, OnLogin);
        smartFox.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
        smartFox.Connect(cfg);
        Debug.Log("connect" + cfg.Host);
    }
	
}
